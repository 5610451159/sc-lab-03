package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.Hash;
import gui.HashFrame;

public class HashTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new HashTest();
	}

	public HashTest() {
		frame = new HashFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setBounds(425, 75, 500, 500);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Hash link = new Hash();
		
		for (int x = 0 ; x < 12 ; x++) {
			if (x == 0) {
				String str = "https://www.google.co.th/" ;
				link.ascii(str) ;
				frame.setResult("1. URL : " + str + "              :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 1) {
				String str = "https://www.facebook.com/" ;
				link.ascii(str) ;
				frame.extendResult("2. URL : " + str + "           :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 2) {
				String str = "http://www.siamphone.com/" ;
				link.ascii(str) ;
				frame.extendResult("3. URL : " + str + "         :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 3) {
				String str = "http://www.sanook.com/" ;
				link.ascii(str) ;
				frame.extendResult("4. URL : " + str + "                :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 4) {
				String str = "http://www.wikipedia.org/" ;
				link.ascii(str) ;
				frame.extendResult("5. URL : " + str + "              :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 5) {
				String str = "http://www.mthai.com/" ;
				link.ascii(str) ;
				frame.extendResult("6. URL : " + str + "                   :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 6) {
				String str = "http://www.ch7.com/" ;
				link.ascii(str) ;
				frame.extendResult("7. URL : " + str + "                       :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 7) {
				String str = "https://www.regis.ku.ac.th/" ;
				link.ascii(str) ;
				frame.extendResult("8. URL : " + str + "            :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 8) {
				String str = "https://www.youtube.com/" ;
				link.ascii(str) ;
				frame.extendResult("9. URL : " + str + "            :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 9) {
				String str = "https://bitbucket.org/" ;
				link.ascii(str) ;
				frame.extendResult("10. URL : " + str + "                    :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 10) {
				String str = "http://www.foodtravel.tv/" ;
				link.ascii(str) ;
				frame.extendResult("11. URL : " + str + "              :: result of mod = " + link.hashing() + "\n");
			}
			
			if (x == 11) {
				String str = "https://www.edmodo.com/" ;
				link.ascii(str) ;
				frame.extendResult("12. URL : " + str + "         :: result of mod = " + link.hashing() + "\n");
			}
			link.setSumAscii() ;
		}
		
	}

	ActionListener list;
	HashFrame frame;
}
